import img1 from "../assets/img/img1.jpg";
import img2 from "../assets/img/img2.jpg";
import img3 from "../assets/img/img3.jpg";
import img4 from "../assets/img/img4.jpg";
import img5 from "../assets/img/img5.jpg";

import theme1 from "../assets/img/theme1.jpg";
import theme2 from "../assets/img/theme2.jpg";
import theme3 from "../assets/img/theme3.jpg";
import theme4 from "../assets/img/theme4.jpg";
import theme5 from "../assets/img/theme5.jpg";
import theme6 from "../assets/img/theme6.jpg";
import theme7 from "../assets/img/theme7.jpeg";
import theme8 from "../assets/img/theme8.jpeg";
import theme9 from "../assets/img/theme9.jpeg";

import gif1 from "../assets/gif/ferris-wheel.gif";
import gif2 from "../assets/gif/fireworks-.gif";
import gif3 from "../assets/gif/fountain.gif";
import gif4 from "../assets/gif/roller-coaster.gif";
import gif5 from "../assets/gif/water-slide.gif";

import icon1 from "../assets/icons/icon1.png";
import icon2 from "../assets/icons/icon2.png";
import icon3 from "../assets/icons/icon3.png";
import icon4 from "../assets/icons/icon4.png";
import icon5 from "../assets/icons/icon5.png";
import icon6 from "../assets/icons/icon6.png";
import icon7 from "../assets/icons/icon7.png";
import icon8 from "../assets/icons/icon8.png";

export const myGifs = [
  { gif: gif1, title: "Открыт круглый год" },
  { gif: gif2, title: "Фейерверки в праздниках" },
  { gif: gif3, title: "Mузыкальные фонтаны" },
  { gif: gif4, title: "Развлечения для всех" },
  { gif: gif5, title: "Большой аквапарк" },
];

export const myImages = [
  {
    image: img1,
    title: "carousel",
  },
  { image: img2, title: "carousel" },
  { image: img3, title: "carousel" },
  { image: img4, title: "carousel" },
  { image: img5, title: "carousel" },
];

export const galleryData = [
  {
    image: img1,
    title: "carousel",
  },
  { image: img2, title: "carousel" },
  { image: img3, title: "carousel" },
  { image: img4, title: "carousel" },
  { image: img5, title: "carousel" },
];
export const carouselItems = [
  {
    image: img1,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 1,
  },
  {
    image: img2,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 2,
  },
  {
    image: img3,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 3,
  },
  {
    image: img4,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 4,
  },
  {
    image: theme1,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 5,
  },
  {
    image: theme2,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 6,
  },
  {
    image: theme3,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 7,
  },
  {
    image: theme4,
    title: "Аттракционы",
    description:
      "В тематическом парке представлено более 35 развлечений на любой вкус",
    id: 8,
  },
];

export const myNews = [
  {
    image: img1,
    title: "My First Blog",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 1,
  },
  {
    image: img2,
    title: "Opening Party!",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 2,
  },
  {
    image: img3,
    title: "Opening Party!",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 3,
  },
  {
    image: img4,
    title: "Opening Party!",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 4,
  },
  {
    image: img3,
    title: "Opening Party!",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 5,
  },
  {
    image: img4,
    title: "Opening Party!",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 6,
  },
  {
    image: img5,
    title: "Opening Party!",
    date: "May 01, 2024",
    description: "Afsona Entertainment Wins 37 Primetime Emmy Awards",
    id: 6,
  },
];

export const themeItems = [
  {
    id: 1,
    image: { item1: theme1, item2: theme9 },
    title: "Аквапарк",
    icon: icon1,
    info: "На территории парка для отдыхающих работает крытый и открытый аквапарк с 10 служебными и 5 техническими зданиями, 16 бассейнами и 7 различными водными аттракционами.",
    description:
      "Откройте для себя волнующие водные аттракционы в нашем аквапарке. Веселье и радость для всей семьи!",
  },
  {
    id: 2,
    image: { item1: theme2, item2: "" },
    title: "Bиртуальная реальность",
    icon: icon2,
    description:
      "Погрузитесь в захватывающий мир виртуальной реальности. Уникальные приключения ждут вас на каждом шагу.",
  },
  {
    id: 3,
    image: { item1: theme3, item2: "" },
    title: "Kвадроцикл",
    icon: icon7,
    description:
      "Почувствуйте адреналин, управляя мощным квадроциклом по нашим трассам. Настоящее вождение в открытом воздухе!",
  },
  {
    id: 4,
    image: { item1: theme4, item2: "" },
    title: "Kолесо обозрения",
    icon: icon5,
    description:
      "Насладитесь великолепными видами с высоты нашего колеса обозрения. Незабываемый опыт для любителей панорам!",
  },
  {
    id: 5,
    image: { item1: theme5, item2: "" },
    title: "Ресторан",
    icon: icon4,
    description:
      "Откройте для себя удивительные вкусы в нашем ресторане. Изысканные блюда и уютная атмосфера ждут вас.",
  },
  {
    id: 6,
    image: { item1: theme6, item2: "" },
    title: "Покупка",
    icon: icon8,
    description:
      "Ознакомьтесь с разнообразным ассортиментом товаров в наших магазинах. Шопинг с удовольствием!",
  },
  {
    id: 7,
    image: { item1: theme7, item2: "" },
    title: "Мечеть",
    icon: icon6,
    description:
      "Посетите нашу красивую мечеть и прочувствуйте атмосферу покоя и духовности.",
  },
  {
    id: 8,
    image: { item1: theme8, item2: "" },
    title: "Фонтан",
    icon: icon3,
    description:
      "Расслабьтесь у нашего фонтана, наслаждаясь великолепным шоу воды и света. Идеальное место для отдыха.",
  },
];
