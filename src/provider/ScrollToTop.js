import { useEffect } from "react";
import useScrollTrigger from "@mui/material/useScrollTrigger";
import Fab from "@mui/material/Fab";
import KeyboardArrowUpIcon from "@mui/icons-material/KeyboardArrowUp";

const ScrollToTop = ({ children }) => {
  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = () => {
    window.scrollTo({
      top: 0,
      behavior: "smooth",
    });
  };

  useEffect(() => {
    const handleScroll = () => {
      if (window.scrollY > 100) {
        // Show the scroll-to-top button when scrolled down
        // You can customize this threshold as needed
        // Set the button to be visible when scrolled down by a certain amount
      }
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <>
      {children}
      <Fab
        size="small"
        aria-label="scroll back to top"
        style={{
          position: "fixed",
          bottom: 16,
          right: 16,
          display: trigger ? "flex" : "none",
          color: "#ffff",
          backgroundImage:
            "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%)",
        }}
        onClick={handleClick}
      >
        <KeyboardArrowUpIcon />
      </Fab>
    </>
  );
};

export default ScrollToTop;
