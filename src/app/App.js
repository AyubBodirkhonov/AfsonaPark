import React, { lazy, Suspense } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import LoadingPage from "../components/loading-page/LoadingPage";

const ScrollToTop = lazy(() => import("../provider/ScrollToTop"));
const NavigationPage = lazy(() => import("../routes/nav/NavigationPage"));
const Home = lazy(() => import("../routes/home/HomePage"));
const AboutPage = lazy(() => import("../pages/about-page/AboutPage"));
const AttractionPage = lazy(() =>
  import("../pages/attraction-page/AttractionPage")
);
const AttractionInfoPage = lazy(() =>
  import("../pages/attraction-page/attraction-info-page/AttractionInfoPage")
);
const ContactPage = lazy(() => import("../pages/contact-page/ContactPage"));
const GalleryPage = lazy(() => import("../pages/gallery-page/GalleryPage"));
const NewsPage = lazy(() => import("../pages/news-page/NewsPage"));
const NewsInfoPage = lazy(() =>
  import("../pages/news-page/news-detail-page/NewsInfoPage")
);
const PartnerPage = lazy(() => import("../pages/partner-page/PartnerPage"));
const FooterPage = lazy(() => import("../routes/footer/FooterPage"));

const ErrorFallback = ({ error }) => (
  <div>
    <h1>Something went wrong!</h1>
    <p>{error.message}</p>
  </div>
);

const App = () => {
  return (
    <Router>
      <Suspense fallback={<LoadingPage />} boundaryFallback={<ErrorFallback />}>
        <ScrollToTop>
          <Routes>
            <Route path="/" element={<NavigationPage />}>
              <Route index element={<Home />} />
              <Route path="about" element={<AboutPage />} />
              <Route path="attraction" element={<AttractionPage />} />
              <Route path="attraction/:id" element={<AttractionInfoPage />} />

              <Route path="contact" element={<ContactPage />} />
              <Route path="gallery" element={<GalleryPage />} />
              <Route path="news" element={<NewsPage />} />
              <Route path="news/:id" element={<NewsInfoPage />} />
              <Route path="partner" element={<PartnerPage />} />
            </Route>
          </Routes>
          <FooterPage />
        </ScrollToTop>
      </Suspense>
    </Router>
  );
};

export default App;
