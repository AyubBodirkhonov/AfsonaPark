import { Grid } from "@mui/material";
import Header from "../../components/header/Header";
import SectionCards from "../../components/section-cards/SectionCards";
import SectionAbout from "../../components/section-about/SectionAbout";
import SectionGallery from "../../components/section-gallery/SectionGallery";
import SectionTheme from "../../components/section-theme/SectionTheme";
import SectionNews from "../../components/section-news/SectionNews";
import SectionPartner from "../../components/section-partner/SectionPartner";

const commonStyle = {
  overflow: "hidden",
  minWidth: "100%",
};
export default function Home() {
  return (
    <Grid container>
      <Grid item xs={12} sx={{ ...commonStyle }}>
        <Header />
      </Grid>
      <Grid item xs={12} sx={{ ...commonStyle, background: "#f9faff" }}>
        <SectionCards />
      </Grid>
      <Grid item xs={12} sx={{ ...commonStyle }}>
        <SectionAbout />
      </Grid>
      <Grid item xs={12} sx={{ ...commonStyle, background: "#f9faff" }}>
        <SectionGallery />
      </Grid>
      <Grid item xs={12} sx={{ ...commonStyle, background: "#f9faff" }}>
        <SectionTheme />
      </Grid>
      <Grid
        item
        xs={12}
        sx={{
          ...commonStyle,
          background: "#f9faff",
          position: "relative",
        }}
      >
        <SectionNews />
      </Grid>
      <Grid
        item
        xs={12}
        sx={{
          position: "relative",
          ...commonStyle,
        }}
      >
        <SectionPartner />
      </Grid>
    </Grid>
  );
}
