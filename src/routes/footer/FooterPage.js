import React from "react";
import {
  Box,
  Typography,
  Stack,
  Container,
  IconButton,
  Divider,
} from "@mui/material";
import { Link } from "react-router-dom";
import Slide from "react-reveal/Slide";
import RoomOutlinedIcon from "@mui/icons-material/RoomOutlined";
import OpenInNewIcon from "@mui/icons-material/OpenInNew";
import EmailOutlinedIcon from "@mui/icons-material/EmailOutlined";
import InstagramIcon from "@mui/icons-material/Instagram";
import TelegramIcon from "@mui/icons-material/Telegram";
import YouTubeIcon from "@mui/icons-material/YouTube";
import AfsonaLogoType from "../../assets/background/afsona-land-high-resolution-logo-white-transparent.png";
import "./FooterPage.style.scss";

const commonStyles = {
  color: "#ffff",
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.8rem", sm: "1rem" },
  fontWeight: "700",
};

const FooterPage = () => {
  return (
    <Box className="footer">
      <div className="footer-dec"></div>
      <Container>
        <Slide top>
          <Box
            sx={{
              width: { xs: "250px", sm: "300px" },
              margin: { xs: "2% auto", sm: "0 auto" },
            }}
            pt={2}
          >
            <Link to="/">
              <img
                src={AfsonaLogoType}
                alt="AfsonaLogotype"
                style={{ width: "100%", height: "100%" }}
              />
            </Link>
          </Box>
        </Slide>
        <Stack
          direction={{ xs: "column", md: "row" }}
          sx={{
            width: { xs: "100%", sm: "500px" },
            margin: { sm: "30px auto" },
          }}
          justifyContent="space-between"
        >
          <Slide left>
            <Stack
              alignItems="center"
              spacing={1}
              sx={{ width: { md: "250px" } }}
              my={{ xs: 2, md: 0 }}
            >
              <IconButton>
                <RoomOutlinedIcon style={{ color: commonStyles.color }} />
              </IconButton>
              <Typography sx={commonStyles}>город Наманган</Typography>
              <Stack direction="row" alignItems="center">
                {" "}
                <Link to="/contact" className="footer-link-a">
                  Наш адрес
                </Link>{" "}
                <IconButton href="/contact">
                  <OpenInNewIcon
                    sx={{
                      width: "18px",
                      height: "18px",
                      color: commonStyles.color,
                    }}
                  />
                </IconButton>
              </Stack>
            </Stack>
          </Slide>
          <Box>
            <Divider
              light
              orientation="vertical"
              sx={{
                background: commonStyles.color,
                marginRight: { xs: "0", md: "25px" },
              }}
            />
          </Box>
          <Divider
            light
            variant="full"
            orientation="horizontal"
            sx={{
              background: commonStyles.color,
              opacity: "0.4",
            }}
          />
          <Slide right>
            <Stack
              alignItems="center"
              spacing={1}
              sx={{ width: { md: "250px" } }}
              my={{ xs: 2, md: 0 }}
            >
              <IconButton>
                <EmailOutlinedIcon style={{ color: commonStyles.color }} />
              </IconButton>
              <Typography sx={commonStyles}>
                afsonalarvodiysi@gmail.com
              </Typography>

              <Stack direction="row" alignItems="center">
                {" "}
                <Link to="/contact" className="footer-link-a">
                  Нашa почта
                </Link>{" "}
                <IconButton href="/contact">
                  <OpenInNewIcon
                    sx={{
                      width: "18px",
                      height: "18px",
                      color: commonStyles.color,
                    }}
                  />
                </IconButton>
              </Stack>
            </Stack>
          </Slide>
        </Stack>
        <Divider
          light
          orientation="horizontal"
          variant="full"
          sx={{
            background: commonStyles.color,
            display: { xs: "block", md: "none" },
            opacity: "0.4",
          }}
        />
        <Slide bottom>
          <Stack
            direction={{ xs: "row" }}
            sx={{ width: { xs: "100%", sm: "700px" }, margin: "0 auto" }}
            justifyContent="center"
            className="footer-pages"
            spacing={2}
            flexWrap="wrap"
            py={3}
          >
            <Link className="footer-link" to="/about">
              О парке
            </Link>

            <Link className="footer-link" to="/attraction">
              Развлечения & Объекты
            </Link>

            <Link className="footer-link" to="/partner">
              Партнеры
            </Link>

            <Link className="footer-link" to="/gallery">
              Галерея
            </Link>

            <Link className="footer-link" to="/news">
              Новости
            </Link>
            <Link className="footer-link" to="/contact">
              Контакты
            </Link>
          </Stack>
        </Slide>
        <Box
          sx={{ margin: "0 auto", width: { xs: "100%", sm: "500px" } }}
          pt={1}
        >
          <Typography
            variant="body2"
            sx={{
              textAlign: "center",
              ...commonStyles,
              fontSize: { xs: "12px", md: "16px" },
            }}
          >
            Afsona land © 2024 Все права защищены
          </Typography>
        </Box>

        <Stack
          alignItems="center"
          justifyContent="center"
          direction="row"
          sx={{ margin: "1% auto", width: { xs: "100%", sm: "500px" } }}
        >
          <IconButton
            href="https://www.instagram.com/afsonalar.vodiysi?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw=="
            target="_blank"
          >
            <InstagramIcon
              style={{
                color: commonStyles.color,
                width: { xs: "24px" },
              }}
            />
          </IconButton>
          <IconButton>
            <YouTubeIcon
              style={{
                color: commonStyles.color,
                width: { xs: "24px" },
              }}
            />
          </IconButton>
          <IconButton>
            <TelegramIcon
              style={{
                color: commonStyles.color,
                width: { xs: "24px" },
              }}
            />
          </IconButton>
        </Stack>
      </Container>
    </Box>
  );
};

export default FooterPage;
