import { useState } from "react";
import { Box, Typography, Divider, Button, Stack } from "@mui/material";
import { Outlet, Link } from "react-router-dom";
import { styled, useTheme } from "@mui/material/styles";
import AfsonaPng from "../../assets/background/afsonalar-manual-removebg-preview.png";
import BackImage from "../../assets/background/global-bg.jpg";
import Drawer from "@mui/material/Drawer";
import AppBar from "@mui/material/AppBar";
import CssBaseline from "@mui/material/CssBaseline";
import IconButton from "@mui/material/IconButton";
import List from "@mui/material/List";
import ListItem from "@mui/material/ListItem";
import ListItemButton from "@mui/material/ListItemButton";
import ListItemText from "@mui/material/ListItemText";
import MenuIcon from "@mui/icons-material/Menu";
import Toolbar from "@mui/material/Toolbar";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import LocationOnIcon from "@mui/icons-material/LocationOn";
import "./NavigationPage.style.scss";

const drawerWidth = "100%";
const navItems = [
  { label: "Главная", link: "/" },
  { label: "О парке", link: "/about" },
  { label: "Развлечения & Объекты", link: "/attraction" },
  { label: "Партнеры", link: "/partner" },
  { label: "Галерея", link: "/gallery" },
  { label: "Новости", link: "/news" },
  { label: "Контакты", link: "/contact" },
];

const drawerStyle = {
  width: "100%",
  height: { xs: "940px", sm: "1200px" },
  position: "absolute",
  top: 0,
  left: 0,
  bottom: 0,
  background: `url('${BackImage}') center no-repeat`,
  backgroundSize: "cover",
  backgroundPosition: "center",
  zIndex: 1,

  "&::before": {
    content: "''",
    position: "absolute",
    display: "block",
    width: "100%",
    height: "100%",
    top: 0,
    left: 0,
    backgroundColor: "#43a4c5",
    backgroundImage:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%)",
    // backgroundImage: "linear-gradient(280deg, #04b6f1 0%, #002cae 62%)",
    opacity: 0.7,
    zIndex: -1,
  },
};

const drawerCSS = {
  width: "100%",
  display: "flex",
  flexDirection: "column",
  height: "250px",
};

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  padding: theme.spacing(0, 1),
  ...theme.mixins.toolbar,
  justifyContent: "flex-end",
}));

const dividerColor = {
  background: "#fff",
};

export default function NavigationPage(props) {
  const { window } = props;
  const [mobileOpen, setMobileOpen] = useState(false);
  const [open, setOpen] = useState(false);

  const handleDrawerToggle = () => {
    setMobileOpen((prevState) => !prevState);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
  const drawer = (
    <Box onClick={handleDrawerToggle} sx={{ ...drawerStyle }}>
      <DrawerHeader sx={{ ...drawerCSS }}>
        <IconButton onClick={handleDrawerClose} sx={{ alignSelf: "flex-end" }}>
          <ChevronLeftIcon sx={{ color: "#fff" }} />
        </IconButton>

        <Box>
          <img
            src={AfsonaPng}
            alt="logo"
            style={{
              width: "100%",
              height: "200px",
              objectFit: "contain",
            }}
          />
        </Box>
      </DrawerHeader>

      <Divider sx={{ ...dividerColor }} />
      <List
        sx={{
          display: "flex",
          flexDirection: "column",
          paddingTop: "0px",
          paddingBottom: "0px",
        }}
      >
        {navItems.map((item, index) => (
          <Box key={index} py={-1}>
            <ListItem disablePadding>
              <ListItemButton href={item.link} className="item-link">
                <ListItemText primary={item.label} className="item-text" />
              </ListItemButton>
            </ListItem>
            {index < navItems.length - 1 && (
              <Divider sx={{ ...dividerColor }} />
            )}
          </Box>
        ))}
      </List>
      <Divider sx={{ ...dividerColor }} />
      <Box mt={5}></Box>
      <Divider sx={{ ...dividerColor }} />
      <List
        sx={{
          display: "flex",
          alignItems: "center",
          textAlign: "start",
          color: "#fff",
          position: "relative",
          fontSize: "0.9rem",
          padding: "0",
          "::after": {
            content: "'❯'",
            position: "absolute",
            top: "20%",
            left: "90%",
            display: "block",
            opacity: "0.5",
          },
        }}
      >
        <ListItem disablePadding>
          <ListItemButton sx={{ padding: "0" }} href="/contact">
            <IconButton>
              <LocationOnIcon sx={{ color: "red", fontSize: "24px" }} />
            </IconButton>
            <ListItemText
              primary="Kарта парка"
              className="item-text"
              sx={{
                span: {
                  fontSize: "14px !important",
                },
              }}
            />
          </ListItemButton>
        </ListItem>
      </List>

      <Divider sx={{ ...dividerColor }} />
    </Box>
  );

  const container =
    window !== undefined ? () => window().document.body : undefined;

  return (
    <>
      <Box>
        <AppBar component="nav">
          <Toolbar className="top-nav" />
        </AppBar>
      </Box>
      <Box className="nav-container">
        <CssBaseline />
        <AppBar component="nav" className="navigation">
          <Toolbar
            sx={{
              display: "flex",
              justifyContent: {
                xs: "flex-start",
                md: "space-between",
                lg: "space-around",
              },
              position: "relative",
              gap: "15px",
            }}
          >
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="start"
              onClick={handleDrawerToggle}
              sx={{
                mr: 2,
                display: { md: "none" },
              }}
            >
              <MenuIcon />
            </IconButton>

            <Stack
              direction="row"
              spacing={3}
              sx={{
                width: { md: "32%", lg: "33%" },
                display: { xs: "none", md: "flex" },
              }}
              justifyContent="flex-end"
            >
              <Link
                to="/about"
                component={Button}
                sx={{ textDecoration: "none", color: "inherit" }}
                className="nav-link"
              >
                О парке
              </Link>
              <Link to="/attraction" className="nav-link">
                Развлечения & Объекты
              </Link>
              <Link to="/partner" className="nav-link">
                Партнеры
              </Link>
            </Stack>
            <Link to="/">
              <Box
                className="nav-logo-container"
                sx={{
                  display: "flex",
                  position: { xs: "absolute", md: "relative" },
                  left: "50%",
                  top: "50%",
                  transform: "translate(-50%, -50%)",
                }}
                mt={{ xs: 2, md: 5 }}
              >
                <span className="nav-logo-text-1"></span>
                <span className="nav-logo-text-2">
                  Afsonalar <br /> Vodiysi
                </span>
                <span className="nav-logo-text-3"></span>
              </Box>
            </Link>
            <Stack
              direction="row"
              spacing={3}
              sx={{
                minWidth: "33%",
                display: { xs: "none", md: "flex" },
              }}
              justifyContent="flex-start"
            >
              <Link to="/gallery" className="nav-link">
                Галерея
              </Link>
              <Link to="/news" className="nav-link">
                Новости
              </Link>
              <Link to="/contact" className="nav-link">
                Контакты
              </Link>
            </Stack>
          </Toolbar>
        </AppBar>
        <nav>
          <Drawer
            container={container}
            variant="temporary"
            open={mobileOpen}
            onClose={handleDrawerToggle}
            ModalProps={{
              keepMounted: true, // Better open performance on mobile.
            }}
            sx={{
              display: { xs: "block", md: "none" },
              "& .MuiDrawer-paper": {
                boxSizing: "border-box",
                width: drawerWidth,
              },
            }}
          >
            {drawer}
          </Drawer>
        </nav>
        <Outlet />
      </Box>
    </>
  );
}
