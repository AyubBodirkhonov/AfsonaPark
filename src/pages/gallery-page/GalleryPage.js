import { Container, Box, Typography } from "@mui/material";
import Slide from "react-reveal/Slide";
import CustomImageList from "../../components/sub-components/custom-image-list/CustomImageList";
import HeaderContent from "../../components/sub-components/header-content/HeaderContent";

const wrapper = {
  position: "absolute",
  display: "block",
  width: "100%",
  left: "0",
  top: "0",
  color: "#eef0f9",
  fontSize: { xs: "5rem", sm: "7rem" },
  fontWeight: "400",
  fontFamily: "Pattaya, sans-serif",
  textAlign: "center",
  lineHeight: "50px",
  transform: "translateY(-50%)",
  letterSpacing: "5px",
  display: { xs: "none", md: "block" },
};

const customText = {
  fontFamily: "Fira Sans Condensed, sans-sarif",
  fontWeight: "700",
  textAlign: "center",
  position: "relative",
  color: "#323268",
  fontSize: { xs: "1.7rem", sm: "2.5rem" },
  "::before": {
    content: '""',
    position: "absolute",
    top: "130%",
    left: "46%",
    height: "4px",
    width: "8%",
    borderRadius: "5px",
    background:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%);",
  },
};
export default function GalleryPage() {
  return (
    <Box sx={{ overflow: "hidden", width: "100%" }}>
      <HeaderContent
        backText="Галерея"
        mainText="Галерея парка"
        link="Главная"
        title="Галерея"
      />
      <Box>
        <Box sx={{ position: "relative", marginTop: { xs: "20%", md: "10%" } }}>
          <Slide left>
            <Typography sx={{ ...wrapper }} variant="h4">
              Галерея
            </Typography>
          </Slide>
          <Slide right>
            <Typography variant="h4" sx={{ ...customText }}>
              Вся галерея
            </Typography>
          </Slide>
        </Box>
        <Box sx={{ margin: { xs: "0 5%", lg: "0 15%" } }}>
          <CustomImageList />
        </Box>
      </Box>
    </Box>
  );
}
