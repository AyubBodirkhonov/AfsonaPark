import { useState } from "react";
import { Typography, Container, Box, Grid } from "@mui/material";
import HeaderContent from "../../components/sub-components/header-content/HeaderContent";
import CustomCard from "../../components/sub-components/card/CustomCard";
import Pagination from "@mui/material/Pagination";
import { myNews } from "../../data/data";
import Slide from "react-reveal/Slide";

const wrapper = {
  position: "absolute",
  display: "block",
  width: "100%",
  left: "0",
  top: "0",
  color: "#eef0f9",
  fontSize: { xs: "5rem", sm: "7rem" },
  fontWeight: "400",
  fontFamily: "Pattaya, sans-serif",
  textAlign: "center",
  lineHeight: "50px",
  transform: "translateY(-50%)",
  letterSpacing: "5px",
  display: { xs: "none", md: "block" },
};

const mainText = {
  textAlign: "center",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  fontWeight: "700",
  color: "#323268",
  position: "relative",
  fontSize: { xs: "1.7rem", sm: "3rem" },
  "::after": {
    content: '""',
    position: "absolute",
    top: { xs: "175%", md: "140%" },
    left: "45%",
    height: "4px",
    width: "10%",
    borderRadius: "5px",
    background: "linear-gradient(90deg, #ffc029 0%, #4cd8b0 100%)",
  },
};
export default function NewsPage() {
  const itemsPerPage = 6;
  const [currentPage, setCurrentPage] = useState(1);

  const handleChangePage = (event, newPage) => {
    setCurrentPage(newPage);
  };

  const startIndex = (currentPage - 1) * itemsPerPage;
  const endIndex = startIndex + itemsPerPage;
  const displayedNews = Array.isArray(myNews)
    ? myNews.slice(startIndex, endIndex)
    : [];
  return (
    <Box sx={{ width: "100%", overflow: "hidden" }}>
      <HeaderContent
        backText="Новости"
        mainText="Новости парка"
        link="Главная"
        title="Новости"
      />
      <Box
        sx={{
          margin: { xs: "15% 5%", md: "5% 20%" },
          display: "flex",
          flexDirection: "column",
        }}
      >
        <Box sx={{ position: "relative", marginTop: { xs: "20%", md: "10%" } }}>
          <span style={wrapper}>Новости</span>
          <Typography sx={{ ...mainText }} variant="h3" mb={7}>
            Свежие Новости
          </Typography>
        </Box>

        <Grid container spacing={2} sx={{ padding: "8px" }}>
          {displayedNews.map((newsItem, index) => (
            <Grid item xs={12} sm={6} md={4} key={index}>
              <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                <CustomCard news={newsItem} />
              </Slide>
            </Grid>
          ))}
        </Grid>
        <Box sx={{ margin: "3% 0", alignSelf: "center" }}>
          <Pagination
            count={Math.ceil(myNews.length / itemsPerPage)}
            color="secondary"
            page={currentPage}
            onChange={handleChangePage}
            size="large"
          />
        </Box>
      </Box>
    </Box>
  );
}
