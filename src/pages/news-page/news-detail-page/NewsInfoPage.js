import { Typography, Box, Container, Stack } from "@mui/material";
import { Outlet, useParams } from "react-router-dom";
import HeaderContent from "../../../components/sub-components/header-content/HeaderContent";
import { myNews } from "../../../data/data";
const commonStyles = {
  fontFamily: "Fira Sans Condensed, sans-sarif",
  fontWeight: "700",
  color: "#323268",
  position: "relative",
  fontSize: { xs: "2rem", sm: "2.5rem" },
  "::after": {
    content: '""',
    position: "absolute",
    width: "20%",
    height: "4px",
    top: "130%",
    left: "40%",
    backgroundImage: "linear-gradient(90deg, #ffc029 0%, #4cd8b0 100%)",
  },
};
const commonText = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", md: "28px" },
  fontWeight: "500",
  letterSpacing: "1px",
  color: "#8b8b9a",
};
const backgroundSettings = {
  width: "100%",
  height: "100%",
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundAttachment: "fixed",
  padding: "10% 0",
  position: "relative",
  filter: "brightness(0.7)",
};
export default function NewsInfoPage() {
  const { id } = useParams();
  const selectedNews = myNews.find((news) => news.id === +id);
  if (!selectedNews) {
    // Handle the case where the news with the specified ID is not found
    return <div>News not found</div>;
  }
  return (
    <>
      <div
        style={{ width: "100vw", background: "#f9faff", overflow: "hidden" }}
      >
        <Box
          sx={{
            width: "100%",
            height: "600px",
            overflow: "hidden",
          }}
        >
          <div
            style={{
              ...backgroundSettings,
              backgroundImage: `url(${selectedNews.image})`,
            }}
          ></div>
        </Box>

        <Container sx={{ margin: "5% auto" }}>
          <Stack direction="column" spacing={3} alignItems="center">
            <Typography
              sx={{
                fontFamily: "Fira Sans Condensed, sans-serif",
                color: "#8b8b9a",
                letterSpacing: "1px",
                fontSize: "1rem",
              }}
              variant="caption"
            >
              {selectedNews.date?.toUpperCase()}
            </Typography>
            <Typography sx={{ ...commonStyles }} variant="h3">
              {selectedNews.title}
            </Typography>
            <Typography sx={{ ...commonText }} pt={3}>
              {selectedNews.description}
            </Typography>
          </Stack>
        </Container>
      </div>
    </>
  );
}
