import { Typography, Box, Container, Stack, Grid } from "@mui/material";
import { Outlet, useParams, Link } from "react-router-dom";
import HeaderContent from "../../../components/sub-components/header-content/HeaderContent";
import { themeItems } from "../../../data/data";
const commonStyles = {
  fontFamily: "Fira Sans Condensed, sans-sarif",
  fontWeight: "700",
  color: "#323268",
  position: "relative",
  textAlign: "center",
  fontSize: { xs: "1.7rem", sm: "3rem" },
  "::after": {
    content: '""',
    position: "absolute",
    width: "10%",
    height: "4px",
    top: "130%",
    left: "45%",
    backgroundImage: "linear-gradient(90deg, #ffc029 0%, #4cd8b0 100%)",
  },
};
const commonText = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", md: "28px" },
  fontWeight: "500",
  letterSpacing: "1px",
  color: "#8b8b9a",
  textAlign: "justify",
};
const backgroundSettings = {
  width: "100%",
  height: "100%",
  backgroundSize: "cover",
  backgroundPosition: "center",
  backgroundRepeat: "no-repeat",
  backgroundAttachment: "fixed",
  padding: "10% 0",
  position: "relative",
  filter: "brightness(0.7)",
};
export default function AttractionInfoPage() {
  const { id } = useParams();
  const selectedItem = themeItems.find((item) => item.id === +id);
  if (!selectedItem) {
    // Handle the case where the news with the specified ID is not found
    return <div>News not found</div>;
  }
  return (
    <>
      <div
        style={{ width: "100vw", background: "#f9faff", overflow: "hidden" }}
      >
        <Box
          sx={{
            width: "100%",
            height: "600px",
          }}
        >
          <div
            style={{
              ...backgroundSettings,
              backgroundImage: `url(${selectedItem.image?.item1})`,
            }}
          ></div>
        </Box>
        <Container sx={{ margin: "5% auto" }}>
          <Box
            sx={{
              width: { xs: "100%", md: "80%" },
              height: "auto",
              overFlow: "hidden",
              margin: "0 auto",
            }}
          >
            <img
              src={selectedItem.image?.item2}
              style={{ width: "100%", height: "auto", borderRadius: "15px" }}
            />
          </Box>
          <Box
            sx={{
              width: "100%",
              display: "flex",
              flexDirection: "column",
              gap: { xs: "15px", md: "30px" },
            }}
            my={2}
          >
            <Typography sx={{ ...commonStyles }} variant="h3">
              {selectedItem.title}
            </Typography>

            <Typography sx={{ ...commonText }} pt={3}>
              {selectedItem.info}
            </Typography>
          </Box>
        </Container>
      </div>
    </>
  );
}
