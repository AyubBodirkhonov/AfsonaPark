import { Container, Box, Typography } from "@mui/material";
import Slide from "react-reveal/Slide";
import HeaderContent from "../../components/sub-components/header-content/HeaderContent";
import GridComponent from "../../components/sub-components/carousel/GridComponent";
import { themeItems } from "../../data/data";
import { responsive } from "../../data/query";
import "react-multi-carousel/lib/styles.css";

const commonStyles = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", md: "28px" },
  fontWeight: "500",
  color: "#8b8b9a",
  letterSpacing: { md: "1px" },
  textAlign: { xs: "justify", md: "center" },
};

const commonText = {
  textAlign: "center",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  fontWeight: "700",
  color: "#323268",
  position: "relative",
  fontSize: { xs: "1.7rem", sm: "3rem" },
  "::before": {
    content: '""',
    position: "absolute",
    top: "-50%",
    left: "45%",
    height: "4px",
    width: "10%",
    borderRadius: "5px",
    background:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%);",
  },
};

export default function AttractionPage(props) {
  console.log("themeItems:", themeItems);
  return (
    <Box>
      <HeaderContent
        backText="Зоны Парка"
        mainText="Развлечения"
        link="Главная"
        title="Развлечения"
      />
      <Box sx={{ margin: { xs: "15% auto", md: "5% auto" } }}>
        <Box
          sx={{
            width: { xs: "90%", md: "70%" },
            margin: "0 auto",
            display: "flex",
            flexDirection: "column",
            gap: "30px",
          }}
        >
          <Slide top>
            <Typography
              sx={{
                ...commonText,
              }}
              variant="h3"
            >
              Зоны Парка
            </Typography>
          </Slide>
          <Slide bottom>
            <Typography
              sx={{
                ...commonStyles,
              }}
            >
              Зоны Парка представляют разнообразие впечатлений от захватывающих
              водных аттракционов до захватывающего мира виртуальной реальности.
              Взрывайте адреналин, управляя квадроциклом, наслаждайтесь высокими
              видами с колеса обозрения, исследуйте удивительные вкусы в
              ресторане, погружайтесь в магазины и ощущайте духовность в
              прекрасной мечети. Парк обещает незабываемые впечатления для
              каждого посетителя.
            </Typography>
          </Slide>
        </Box>
      </Box>
      <GridComponent
        themeItems={themeItems}
        responsive={responsive}
        sx={{ margin: "5% auto" }}
      />
    </Box>
  );
}
