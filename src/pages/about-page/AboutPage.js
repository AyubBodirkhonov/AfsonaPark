import { Typography, Container, Box, Grid, Stack, Card } from "@mui/material";
import HeaderContent from "../../components/sub-components/header-content/HeaderContent";
import HeaderImg from "../../assets/img/theme8.jpeg";
import Afsonalar from "../../assets/background/land.jpeg";
import Masjid from "../../assets/background/masjid.jpg_large";
import Slide from "react-reveal/Slide";

import "./AboutPage.style.scss";

const commonStyles = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", md: "28px" },
  fontWeight: "500",
  letterSpacing: "1px",
  color: "#8b8b9a",
  textAlign: { xs: "justify", md: "center" },
  padding: "10px",
};

const commonText = {
  textAlign: "center",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  fontWeight: "700",
  color: "#323268",
  fontSize: { xs: "1.7rem", lg: "3rem" },
};

export default function AboutPage() {
  return (
    <Box
      sx={{
        overflow: "hidden",
        background: "#f9faff",
        width: "100%",
      }}
    >
      <HeaderContent
        backText="Парк"
        mainText="О парке"
        link="Главная"
        title="О парке"
      />
      <Grid container>
        <Grid item xs={12} sx={{ padding: "5% 0" }}>
          <Container>
            <Box
              sx={{
                width: { xs: "100%", md: "70%" },
                margin: "0 auto",
                display: "flex",
                flexDirection: "column",
                gap: "30px",
              }}
            >
              <Slide top>
                <Typography
                  sx={{
                    ...commonText,
                  }}
                  variant="h3"
                  className="about-page-text"
                >
                  Наша Миссия
                </Typography>
              </Slide>
              <Slide bottom>
                <Typography
                  sx={{
                    ...commonStyles,
                  }}
                >
                  Миссия Afsona Land — увлечь людей всех возрастов впечатлениями
                  от парка развлечений мирового класса, создавая волшебное
                  царство, где воображение не знает границ. Благодаря
                  инновационным аттракционам и исключительному гостеприимству мы
                  стремимся стать лучшим местом для радости и смеха и
                  незабываемые воспоминания.
                </Typography>
              </Slide>
            </Box>
          </Container>
        </Grid>
        <Grid
          item
          xs={12}
          sx={{
            backgroundImage: `url(${HeaderImg})`,
            backgroundSize: "cover",
            backgroundPosition: "center",
            backgroundAttachment: "fixed",
            padding: "10% 0",
            position: "relative",
          }}
        >
          <span className="filter"></span>
          <Container>
            <Box
              sx={{
                width: "80%",
                margin: "0 auto",
                display: "flex",
                flexDirection: { xs: "column", md: "row" },
                gap: { xs: "30px", md: "80px" },
              }}
            >
              <Slide left>
                <Typography
                  sx={{
                    textAlign: "center",
                    fontFamily: "Fira Sans Condensed, sans-sarif",
                    fontWeight: "00",
                    fontSize: { xs: "1.7rem", sm: "3rem" },
                    color: "#fff",
                  }}
                  variant="h2"
                  className="about-page-text-2"
                  pt={1}
                >
                  Запущен
                </Typography>
              </Slide>
              <Slide right>
                <Typography
                  sx={{
                    fontSize: { xs: "0.8rem", sm: "1rem" },
                    fontWeight: "400",
                    fontFamily: "Fira Sans Condensed, sans-sarif",
                    color: "#fff",
                    zIndex: "1",
                    textAlign: { xs: "justify", md: "start" },
                  }}
                  p={{ xs: 2, md: 3 }}
                >
                  Во время посещения Наманганской области в 2018 году Президент
                  Шавкат Мирзиёев ознакомился с проектом этого парка культуры и
                  отдыха и предложил назвать его «Афсоналар водийси» – Долина
                  легенд.
                </Typography>
              </Slide>
            </Box>
          </Container>
        </Grid>
        <Grid item xs={12}>
          <Box
            sx={{
              padding: "5% 0",
              width: "80%",
              margin: "0 auto",
            }}
          >
            <Stack
              direction={{ xs: "column", md: "row" }}
              justifyContent="center"
              spacing={5}
            >
              <Slide top left>
                <Card
                  sx={{
                    width: { xs: "100%", md: "90%" },
                    height: { xs: "400px", md: "600px" },
                    borderRadius: "15px",
                  }}
                >
                  <img
                    src={Afsonalar}
                    alt="Masjid"
                    style={{
                      width: "100%",
                      height: "100%",
                      objectFit: "cover",
                    }}
                  />
                </Card>
              </Slide>
              <Box
                sx={{
                  width: { xs: "100%", lg: "80%" },
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  gap: "20px",
                }}
                mt={5}
              >
                <Slide top>
                  <Typography
                    sx={{
                      ...commonText,
                    }}
                    variant="h3"
                    pt={1}
                    className="about-page-text"
                  >
                    Парк Долина легенд
                  </Typography>
                </Slide>

                <Slide left>
                  <Typography
                    sx={{
                      ...commonStyles,
                      textAlign: "justify",
                      display: { xs: "none", lg: "block" },
                    }}
                    mt={5}
                  >
                    Для посетителей на территории парка действует крытый и
                    открытый аквапарк, где готовы к эксплуатации 10 сервисных и
                    5 технических корпусов, 16 бассейнов и 7 видов водных
                    аттракционов.
                  </Typography>
                </Slide>
                <Slide right>
                  <Typography
                    sx={{
                      ...commonStyles,
                      textAlign: {
                        xs: "justify",
                        md: "justify",
                      },
                      padding: { xs: "none", md: "10px" },
                    }}
                  >
                    Строительство парка было реализовано отечественными
                    предпринимателями совместно с турецкой проектной компанией
                    «DOME+Partners».Для удобства посетителей на территории
                    комплекса «Afsonalar vodiysi» действуют автостоянка,
                    рассчитанная на 1300 автомобилей, двухэтажный торговый центр
                    со 112 магазинами, тематический парк и аттракционы с 16
                    видами современных аттракционов, виртуальные игровые
                    комнаты, а также рестораны, обслуживающие до 1000
                    посетителей.
                  </Typography>
                </Slide>
                <Slide left>
                  <Typography
                    sx={{
                      ...commonStyles,
                      textAlign: "justify",
                      display: { xs: "none", md: "block" },
                    }}
                  >
                    Уютный амфитеатр на 2400 мест и летний амфитеатр станут
                    главной площадкой для проведения различных развлекательных
                    мероприятий Наманганской области.
                  </Typography>
                </Slide>
              </Box>
            </Stack>
          </Box>
        </Grid>

        <Grid item xs={12} sx={{ backgroundColor: "#fff !important" }}>
          <Box
            sx={{
              padding: "5% 0",
              width: "80%",
              margin: "0 auto",
            }}
          >
            <Stack
              direction={{ xs: "column", md: "row" }}
              justifyContent="center"
              spacing={5}
            >
              <Box
                sx={{
                  width: { xs: "100%", md: "80%" },
                  display: "flex",
                  flexDirection: "column",
                  alignItems: "center",
                  gap: "50px",
                }}
                mt={5}
              >
                <Slide top>
                  <Typography
                    sx={{
                      ...commonText,
                    }}
                    variant="h3"
                    pt={1}
                    className="about-page-text"
                  >
                    Мечеть в Долине Афсона
                  </Typography>
                </Slide>

                <Slide left>
                  <Typography
                    sx={{
                      ...commonStyles,
                      textAlign: "justify",
                    }}
                  >
                    На территории тематического парка располагается самый
                    крупный 3-этажный центр ремесленников, мечеть вместимостью
                    до 15000 человек, торговый центр, современная гостиница и
                    виллы. Комплекс также украшают современные водопады,
                    музыкальные фонтаны, каналы и озера, окруженные
                    декоративными деревьями и скамейками.
                  </Typography>
                </Slide>
              </Box>
            </Stack>
            <Slide left>
              <Stack mt={{ md: 5 }}>
                <Card
                  sx={{
                    width: { xs: "100%", md: "100%" },
                    height: { xs: "400px", md: "600px" },
                    borderRadius: "15px",
                  }}
                >
                  <img
                    src={Masjid}
                    alt="Masjid"
                    style={{
                      width: "100%",
                      height: "100%",
                      objectFit: "cover",
                    }}
                  />
                </Card>
              </Stack>
            </Slide>
          </Box>
        </Grid>
      </Grid>
    </Box>
  );
}
