import HeaderContent from "../../components/sub-components/header-content/HeaderContent";
import { Box, Grid, Typography, Card, Stack } from "@mui/material";
import Slide from "react-reveal/Slide";
import PartnerKarzinka from "../../assets/img/partner1.jpg";
import PartnerClick from "../../assets/img/partner2.png";

const commonStyles = {
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  color: "#323268",
  textAlign: "center",
  position: "relative",
  fontSize: { xs: "1.5rem", xl: "2rem" },
  "::before": {
    content: '""',
    position: "absolute",
    top: { xs: "120%", lg: "140%" },
    left: { xs: "43%", lg: "40%" },
    height: "4px",
    width: { xs: "10%", lg: "14%" },
    borderRadius: "5px",
    background:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%)",
  },
};

const commonText = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", sm: "30px" },
  fontWeight: "500",
  letterSpacing: "1px",
  color: "#8b8b9a",
  textAlign: "justify",
};

const commonCard = {
  width: { xs: "100vw", lg: "80%" },
  height: { xs: "400px", md: "535px" },
  zIndex: "99",
  borderRadius: { lg: "30% 70% 70% 30% / 30% 30% 70% 70%" },
  border: { lg: "5px solid #e4002b" },
  margin: { lg: "20px auto" },
  backgroundImage: `url(${PartnerKarzinka})`,
  transition: "0.4s ease-in-out",
  cursor: "pointer",
  transform: "translateY(0px)",
  ":hover": {
    transform: "translateY(-10px)",
  },
};
export default function PartnerPage() {
  return (
    <Box sx={{ overflow: "hidden" }}>
      <HeaderContent
        backText="Партнеры"
        mainText="Партнеры"
        link="Главная"
        title="Партнеры"
      />
      <Box sx={{ padding: { md: "5% 0" }, background: "#f9faff" }}>
        <Grid container justifyContent="center" spacing={6}>
          <Grid item xs={12} lg={5}>
            <Slide left>
              <Card
                sx={{
                  ...commonCard,
                }}
              >
                <img
                  src={PartnerKarzinka}
                  alt="Korzinka"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Card>
            </Slide>
          </Grid>
          <Grid item xs={12} lg={5}>
            <Slide right>
              <Stack sx={{ marginTop: { xs: "10%", sm: "2%" } }}>
                <Typography
                  variant="h4"
                  sx={{
                    ...commonStyles,
                  }}
                  px={{ xs: 3, md: 0 }}
                >
                  Один из лучших супермаркетов
                  <br /> Узбекистана{" "}
                  <span style={{ color: "#e4002b" }}>Кoрзинка</span>
                </Typography>
                <Typography
                  sx={{
                    ...commonText,
                    marginTop: { xs: "10%", sm: "4%", lg: "13%" },
                  }}
                  p={{ xs: 3, lg: 0 }}
                >
                  В Afsona Land мы с гордостью сообщаем о нашем уважаемом
                  партнерстве с супермаркетом Korzinka. Вместе мы разделяем
                  стремление предоставить нашему сообществу исключительный опыт
                  и услуги. Наше сотрудничество направлено на повышение общего
                  удовольствия и удобства для наших посетителей. Будучи
                  партнерами, Afsona Land и супермаркет Korzinka стремятся
                  создавать незабываемые моменты, предлагать качественные
                  продукты и создавать гостеприимную атмосферу для всех. Мы
                  надеемся на успешное партнерство и дальнейшее превосходство в
                  служении нашему сообществу.
                </Typography>
              </Stack>
            </Slide>
          </Grid>
        </Grid>
      </Box>

      {/* Partner Content 2 */}
      <Box>
        <Grid container justifyContent="center" spacing={6}>
          <Grid
            item
            xs={12}
            lg={5}
            sx={{ display: { xs: "block", lg: "none" } }}
          >
            <Card
              sx={{
                ...commonCard,
                overflow: "visible",
                background: "#181926",
                border: { lg: "5px solid #00b1e3" },
              }}
            >
              <Slide right>
                <img
                  src={PartnerClick}
                  alt="Click"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "contain",
                  }}
                />
              </Slide>
            </Card>
          </Grid>
          <Grid item xs={12} lg={5}>
            <Slide left>
              <Stack sx={{ marginTop: { xs: "10%", sm: "2%", lg: "10%" } }}>
                <Typography
                  variant="h4"
                  px={{ xs: 4, lg: "0" }}
                  sx={{
                    ...commonStyles,
                  }}
                >
                  Одна из ведущих платежных систем <br /> Узбекистана{" "}
                  <span style={{ color: "#00b1e3" }}>Click</span>
                </Typography>
                <Typography
                  sx={{
                    ...commonText,
                    marginTop: { xs: "10%", sm: "4%", lg: "13%" },
                  }}
                  p={{ xs: 3, lg: 0 }}
                >
                  Рады отметить годы партнерства с Click.uz! Afsona Land и
                  Click.uz вместе создают воспоминания в течение многих лет,
                  предлагая беспрепятственные платежи за наши
                  достопримечательности. Мы надеемся на успешное партнерство и
                  дальнейшее превосходство в служении нашему сообществу.
                </Typography>
              </Stack>
            </Slide>
          </Grid>
          <Grid
            item
            xs={12}
            lg={5}
            sx={{ display: { xs: "none", lg: "block" } }}
          >
            <Card
              sx={{
                ...commonCard,
                overflow: "visible",
                background: "#181926",
                border: { lg: "5px solid #00b1e3" },
              }}
            >
              <Slide right>
                <img
                  src={PartnerClick}
                  alt="Click"
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "contain",
                  }}
                />
              </Slide>
            </Card>
          </Grid>
        </Grid>
      </Box>
    </Box>
  );
}
