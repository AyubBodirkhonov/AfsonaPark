import {
  Typography,
  Container,
  Box,
  Stack,
  Card,
  TextField,
  Grid,
  FormControl,
  Input,
} from "@mui/material";
import InputAdornment from "@mui/material/InputAdornment";
import PersonIcon from "@mui/icons-material/Person";
import CallIcon from "@mui/icons-material/Call";
import EmailIcon from "@mui/icons-material/Email";
import TelegramIcon from "@mui/icons-material/Telegram";
import Slide from "react-reveal/Slide";
import CustomButton from "../../components/sub-components/button/CustomButton";
import HeaderContent from "../../components/sub-components/header-content/HeaderContent";
import "./ContactPage.style.scss";

const commonInputStyle = {
  "& .MuiOutlinedInput-notchedOutline": {
    border: "1px solid #eef0f9",
    boxShadow: "5px 5px 10px 0 #eef0f9",
    fontSize: "14px",
  },
  "& .MuiInputLabel-root": {
    fontSize: { xs: "14px", md: "16px" },
  },
};

export default function ContactPage() {
  return (
    <Box sx={{ width: "100%", overflow: "hidden" }}>
      <HeaderContent
        backText="Контакты"
        mainText="Контакты парка"
        link="Главная"
        title="Контакты"
      />
      <Box
        sx={{
          background: "#f9faff",
          padding: { xs: "1rem 0", md: "4rem" },
        }}
      >
        <Box sx={{ margin: { xs: "0 5%", lg: "0 15%" } }}>
          <Slide left>
            <Box className="responsive-map">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3011.014998372558!2d71.61574247656915!3d41.00304491974626!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38bb4b980dc68ce9%3A0x6f36578d3ae2778a!2sAfsonalar%20vodiysi%2C%20Namangan!5e0!3m2!1sen!2sus!4v1704790973203!5m2!1sen!2sus"
                loading="lazy"
                referrerPolicy="no-referrer-when-downgrade"
              ></iframe>
            </Box>
          </Slide>
          <Box
            sx={{
              position: "relative",
              margin: { xs: "25% 0", md: "8% 0" },
            }}
          >
            <Slide left>
              <span className="contact-wrap-text">Напишите Нам</span>
            </Slide>
            <Slide right>
              <Typography
                className="contact-text-topline"
                variant="h4"
                sx={{
                  fontFamily: "Fira Sans Condensed, sans-sarif",
                  fontWeight: "700",
                  textAlign: "center",
                  position: "relative",
                  "::before": {
                    content: '""',
                    position: "absolute",
                    top: "-90%",
                    left: "45%",
                    height: "4px",
                    width: "10%",
                    borderRadius: "5px",
                    background:
                      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%);",
                  },
                }}
                color="#323268"
              >
                Связаться с Нами
              </Typography>
            </Slide>
          </Box>

          {/* Form ------------------------------------- */}

          <Box
            component="form"
            sx={{
              "& .MuiTextField-root": { m: 1, width: "25ch" },
            }}
            noValidate
            autoComplete="off"
          >
            <Slide bottom>
              <Grid container>
                <Grid item xs={12} md={6} xl={12}>
                  <TextField
                    id="input-with-icon-textfield"
                    label="Ваше имя"
                    type="text"
                    sx={{
                      minWidth: { xs: "95%", md: "90%", xl: "32%" },
                      ...commonInputStyle,
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <PersonIcon />
                        </InputAdornment>
                      ),
                    }}
                  />

                  <TextField
                    id="outlined-required"
                    label="Ваш телефон"
                    type="number"
                    sx={{
                      minWidth: { xs: "95%", md: "90%", xl: "31%" },
                      ...commonInputStyle,
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <CallIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                  <TextField
                    id="outlined-required"
                    label="Ваша почта"
                    type="email"
                    sx={{
                      minWidth: { xs: "95%", md: "90%", xl: "32%" },
                      ...commonInputStyle,
                    }}
                    InputProps={{
                      endAdornment: (
                        <InputAdornment position="end">
                          <EmailIcon />
                        </InputAdornment>
                      ),
                    }}
                  />
                </Grid>

                <Grid item xs={12} md={6} xl={12}>
                  <TextField
                    id="standard-textarea"
                    type="text"
                    placeholder="Ваш комментарий"
                    multiline
                    label="Ваш комментарий"
                    rows={4}
                    sx={{
                      minWidth: { xs: "95%", md: "98%" },
                      ...commonInputStyle,
                    }}
                  />
                </Grid>
                <Grid item xs={12} md={4} mt={1} pb={2}>
                  <CustomButton
                    variant="contained"
                    sx={{
                      padding: { xs: "12px 20px", md: "14px" },
                      boxShadow: "0 4px 15px 0 rgba(126, 52, 161, 0.75)",
                      borderRadius: "16px",
                      fontSize: { xs: "12px", md: "14px !important" },
                      fontFamily: "Fira Sans Condensed, sans-sarif",
                      fontWeight: "700",
                      width: { xs: "50%", sm: "25%", md: "75%", lg: "50%" },
                      marginLeft: "8px",
                    }}
                  >
                    Отправить сообщение
                  </CustomButton>
                </Grid>
              </Grid>
            </Slide>
          </Box>
        </Box>
      </Box>
    </Box>
  );
}
