import { Typography, Box } from "@mui/material";
import { Link } from "react-router-dom";
import Slide from "react-reveal/Slide";
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { themeItems } from "../../data/data";
import { responsive } from "../../data/query";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import "./SectionTheme.style.scss";
import CustomButton from "../sub-components/button/CustomButton";

export default function SectionTheme() {
  return (
    <Box className="section-theme">
      <Box className="slideDec"></Box>
      <Slide top>
        <Box className="slide-wrap-container">
          <span className="slide-wrap-text">Развлечения</span>
          <Typography
            className="slide-top-text"
            variant="h4"
            sx={{
              fontFamily: "Fira Sans Condensed, sans-sarif",
              fontWeight: "700",
              marginLeft: { md: "1rem" },
              fontSize: { xs: "1.5rem", lg: "3rem" },
            }}
          >
            Тематические Зоны Парка
          </Typography>
        </Box>
      </Slide>
      <Slide top>
        <Carousel
          responsive={responsive}
          swipeable
          infinite
          itemClass="carousel-item"
        >
          {themeItems.map((item, index) => (
            <Box key={index} className="carousel-box">
              <img
                src={item.image?.item1}
                alt={`img${index + 1}`}
                className="theme-image"
              />
              <Box className="carousel-text-box">
                <img src={item?.icon} />
                <Typography variant="h2">{item?.title}</Typography>
                <Typography variant="body1">{item?.description}</Typography>
                <Link to={`/attraction/${item?.id}`}>
                  <CustomButton
                    endIcon={<ChevronRightIcon />}
                    variant="outlined"
                    sx={{
                      color: "#fff",
                      background: "transparent !important",
                      border: "1px solid #fff",
                      borderRadius: "10px",
                      fontSize: { xs: "14px", md: "16px" },
                      "&:hover": {
                        border: "1px solid transparent",
                      },
                    }}
                  >
                    Подробнее
                  </CustomButton>
                </Link>
              </Box>
            </Box>
          ))}
        </Carousel>
      </Slide>
    </Box>
  );
}
