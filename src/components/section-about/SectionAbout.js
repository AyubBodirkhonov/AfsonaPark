import { Box, Grid, Typography, Card, Stack } from "@mui/material";
import Slide from "react-reveal/Slide";
import WheelImg from "../../assets/img/img3.jpg";
import Wheel from "../../assets/icons/wheel.png";
import Carousel from "../../assets/icons/carousel.png";
import "./SectionAbout.style.scss";
import CustomButton from "../sub-components/button/CustomButton";
import { Link } from "react-router-dom";

///////////////////////// Custom Style
const commonStyles = {
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  color: "#323268",
  textAlign: { xs: "center", md: "start" },
};

const typoStyle = {
  position: "relative",
  fontSize: { xs: "1.3rem", md: "2.125rem" },
  textAlign: { xs: "center", md: "start" },

  "::before": {
    content: '""',
    position: "absolute",
    top: { xs: "-20%", sm: "-50%", md: "-20%" },
    left: { xs: "42%", md: "0" },
    height: "4px",
    width: { xs: "17%", md: "15%" },
    borderRadius: "5px",
    background:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%)",
  },
};

const commonText = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", md: "28px" },
  fontWeight: "500",
  color: "#8b8b9a",
  textAlign: { xs: "justify", md: "start" },
  marginTop: "10px",
};

const buttonStyle = {
  padding: { xs: "10px 16px", md: "14px 16px" },
  boxShadow: "0 4px 15px 0 rgba(126, 52, 161, 0.75)",
  borderRadius: "24px",
  fontSize: "0.7rem",
  width: { xs: "60%", sm: "30%" },
};
export default function SectionAbout() {
  return (
    <Box className="section-about">
      <div className="about-dec"></div>
      <Grid container justifyContent="center">
        <Grid item xs={12} lg={6} mt={{ xs: 4, lg: 0 }}>
          <Slide top left>
            <Card
              sx={{
                width: { xs: "80%", md: "70%" },
                height: { xs: "300px", md: "635px" },
                zIndex: "99",
                borderRadius: "15px",
                margin: "0 auto",
              }}
            >
              <img
                src={WheelImg}
                style={{ width: "100%", height: "100%", objectFit: "cover" }}
              />
            </Card>
          </Slide>
        </Grid>
        <Grid item xs={12} md={8} lg={4} p={{ xs: 3, sm: 6, lg: 0 }}>
          <Slide top right>
            <Stack mt={2}>
              <Typography
                variant="h4"
                sx={{
                  ...commonStyles,
                  ...typoStyle,
                }}
              >
                Крупнейший В Средней Азии Всесезонный Парк Развлечений и
                Аттракционов Afsona Land
              </Typography>
              <Stack
                direction={{ xs: "column", md: "row" }}
                alignItems={{ xs: "center", md: "flex-start" }}
                spacing={{ xs: 0, md: 4 }}
                my={3}
              >
                <Box>
                  <img
                    src={Wheel}
                    alt="wheel icon"
                    sx={{ width: "100%", height: "100%" }}
                  />
                </Box>
                <Stack spacing={{ md: 1 }}>
                  <Typography sx={{ ...commonStyles }} variant="h6">
                    Тематический Парк
                  </Typography>
                  <Typography sx={{ ...commonText }}>
                    В Узбекистане открыт уникальный тематический парк Afsona
                    Land. Здесь каждый день — настоящий карнавал эмоций и
                    волшебных воспоминаний. Радость и улыбки наполняют этот
                    удивительный мир развлечений. Погрузитесь в веселье Afsona
                    Land!
                  </Typography>
                </Stack>
              </Stack>
              <Stack
                direction={{ xs: "column", md: "row" }}
                spacing={{ xs: 0, md: 4 }}
                my={3}
                alignItems={{ xs: "center", md: "flex-start" }}
              >
                <Box>
                  <img
                    src={Carousel}
                    alt="wheel icon"
                    sx={{ width: "100%", height: "100%" }}
                  />
                </Box>
                <Stack spacing={{ md: 1 }}>
                  <Typography sx={{ ...commonStyles }} variant="h6">
                    Долина Легенд
                  </Typography>
                  <Typography sx={{ ...commonText }}>
                    Во время посещения Наманганской области в 2018 году
                    Президент Шавкат Мирзиёев ознакомился с проектом этого парка
                    культуры и отдыха и предложил назвать его «Афсоналар
                    водийси» – Долина легенд.
                  </Typography>
                </Stack>
              </Stack>
              <Link to="/about" style={{ textAlign: "center" }}>
                <CustomButton
                  variant="contained"
                  sx={{
                    ...buttonStyle,
                  }}
                >
                  <strong>Подробнее</strong>
                </CustomButton>
              </Link>
            </Stack>
          </Slide>
        </Grid>
      </Grid>
    </Box>
  );
}
