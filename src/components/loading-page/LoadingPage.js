import { Box } from "@mui/material";
import "./Loading.style.scss";
export default function LoadingPage() {
  return (
    <Box className="loading-container">
      <Box className="loading-spinner" />
    </Box>
  );
}
