import { Box, Card, Stack, Typography, Grid } from "@mui/material";
import Korzinka from "../../assets/gif/karzinka.png";
import Click from "../../assets/gif/Click-01.png";
import Slide from "react-reveal/Slide";
import { Link } from "react-router-dom";
import "./SectionPartner.style.scss";

const commonStyles = {
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  color: "#323268",
};

const commonCard = {
  width: "100%",
  maxHeight: "200px",
  display: "flex",
  alignItems: "center",
  borderRadius: "15px",
  boxShadow: "10px 0 25px 5px #eef0f9",
  transition: "0.4s ease-in-out",
  transform: "translateY(0)",
  cursor: "pointer",
  ":hover": {
    transform: "translateY(-10px)",
  },
};

const wrap = {
  position: "absolute",
  display: "block",
  width: "100%",
  left: "0",
  top: "24%",
  color: "#eef0f9",
  fontSize: "90px",
  fontWeight: "400",
  fontFamily: "Pattaya, sans-serif",
  textAlign: "center",
  lineHeight: "50px",
  transform: "translateY(-50%)",
  letterSpacing: "10px",
};

export default function SectionPartner() {
  return (
    <Box
      sx={{
        width: "100%",
        padding: { xs: "7% 5%", sm: "0 3%", md: "5% 20%" },
      }}
    >
      <Grid
        container
        direction="row"
        sx={{ margin: { xs: "15% auto", md: "5% auto" } }}
        justifyContent="center"
      >
        <Grid item xs={12}>
          <Slide top>
            <Box
              sx={{
                position: "relative",
              }}
            >
              <span style={wrap} className="wrapper">
                Партнёры
              </span>

              <Typography
                variant="h3"
                sx={{
                  textAlign: "center",
                  ...commonStyles,
                  position: "relative",
                  fontSize: { xs: "1.8rem", lg: "3rem" },
                  "::before": {
                    content: '""',
                    position: "absolute",
                    top: "-35%",
                    left: { xs: "42%", md: "47%" },
                    height: "4px",
                    width: { xs: "15%", md: "5%" },
                    borderRadius: "5px",
                    background:
                      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%);",
                  },
                }}
              >
                Партнёры <br />
              </Typography>
            </Box>
          </Slide>
        </Grid>

        <Grid item xs={12} sx={{ marginTop: { xs: "15%", md: "5%" } }}>
          <Stack
            direction="row"
            alignItems="center"
            justifyContent="center"
            spacing={{ xs: 2, md: 10 }}
          >
            <Slide left>
              <Link to="/partner">
                <Card
                  sx={{
                    ...commonCard,
                  }}
                >
                  <img
                    src={Korzinka}
                    alt="Korzinka"
                    style={{
                      width: "100%",
                      height: "300px",
                      objectFit: "contain",
                    }}
                  />
                </Card>
              </Link>
            </Slide>
            <Slide right>
              <Link to="/partner">
                <Card
                  sx={{
                    ...commonCard,
                  }}
                >
                  <img
                    src={Click}
                    alt="Click"
                    style={{
                      width: "100%",
                      height: "300px",
                      objectFit: "contain",
                    }}
                  />
                </Card>
              </Link>
            </Slide>
          </Stack>
        </Grid>
      </Grid>
    </Box>
  );
}
