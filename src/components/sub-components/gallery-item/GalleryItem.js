import React from "react";
import { Box, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import CustomButton from "../button/CustomButton";
import "./GalleryItem.style.scss";

const GalleryItem = ({ image, title }) => (
  <Box className="gallery-box">
    <img src={image} alt="Gallery item" className="carousel-image" />
    <div className="overlay">
      <div className="gallery-in">
        <Typography className="gallery-in-text">{title}</Typography>
        <CustomButton
          variant="contained"
          sx={{
            padding: "10px 16px",
            boxShadow: "0 4px 15px 0 rgba(126, 52, 161, 0.75)",
            borderRadius: "24px",
            fontSize: "0.7rem",
            display: { xs: "none", md: "block" },
          }}
        >
          <Link to="/gallery">
            <strong>Подробнее</strong>
          </Link>
        </CustomButton>
      </div>
    </div>
  </Box>
);

export default GalleryItem;
