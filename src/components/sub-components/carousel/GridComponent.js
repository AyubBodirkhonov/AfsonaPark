import { Grid, Container, Box, Typography } from "@mui/material";
import { Link } from "react-router-dom";
import Slide from "react-reveal/Slide";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
import CustomButton from "../button/CustomButton";
import "./GridComponent.style.scss";
import { typography } from "@mui/system";

export default function GridComponent({ themeItems, ...props }) {
  return (
    <Container {...props}>
      <Grid container spacing={2}>
        {themeItems.map((item, index) => (
          <Grid
            item
            xs={12}
            md={
              index === 0 ? 8 : 4 && index === 3 ? 8 : 4 && index === 7 ? 12 : 4
            }
            key={item.id}
          >
            <Slide left={index % 2 === 0} bottom={index % 2 === 1}>
              <Box className="theme-container-card">
                <Link to={`/attraction/${item.id}`}>
                  <img
                    src={item?.image?.item1}
                    alt={item?.title}
                    className="itemImage"
                  />
                </Link>
                <Box className="theme-text-box">
                  <Box>
                    <img src={item?.icon} />
                    <Typography variant="h2">{item?.title}</Typography>
                  </Box>

                  <Typography variant="body2">{item?.description}</Typography>
                  <Link to={`/attraction/${item?.id}`}>
                    <CustomButton
                      endIcon={<ChevronRightIcon />}
                      variant="outlined"
                      sx={{
                        color: "#fff",
                        background: "transparent !important",
                        border: "1px solid #fff",
                        borderRadius: "10px",
                        fontSize: { xs: "14px", md: "16px" },
                        marginTop: "5px",
                        "&:hover": {
                          border: "1px solid transparent",
                        },
                      }}
                    >
                      Подробнее
                    </CustomButton>
                  </Link>
                </Box>
              </Box>
            </Slide>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
}
