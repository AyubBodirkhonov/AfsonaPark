import { Card, Typography, Stack, Box } from "@mui/material";
import { Link } from "react-router-dom";

const commonStyles = {
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  color: "#323268",
};

const commonText = {
  fontFamily: "Fira Sans Condensed, sans-serif",
  fontSize: { xs: "0.7rem", sm: "0.9rem" },
  lineHeight: { xs: "24px", md: "28px" },
  fontWeight: "500",
  letterSpacing: "1px",
  color: "#8b8b9a",
  textAlign: "center",
};

export default function CustomCard({ news }) {
  return (
    <>
      <Card
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          flexDirection: "column",
          width: { xs: "100%", sm: "300px", lg: "350px" },
          height: { md: "415px" },
          transition: "0.4s ease-in-out",
          cursor: "pointer",
          boxShadow: "4px 4px 10px rgba(0, 0, 0, .08)",
          transform: "translateY(0px)",
          ":hover": {
            transform: "translateY(-10px)",
          },
          margin: "2% 0",
        }}
      >
        <Link to={`/news/${news.id}`}>
          <Box sx={{ width: "100%", height: { xs: "150px", sm: "240px" } }}>
            <img
              src={news?.image}
              alt={news?.title}
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </Box>

          <Stack justifyContent="center" alignItems="center" p={3}>
            <Typography variant="caption" sx={{ ...commonText }}>
              {news?.date}
            </Typography>
            <Typography variant="h6" sx={{ ...commonStyles }}>
              {news?.title}
            </Typography>
            <Typography sx={{ ...commonText }}>{news.description}</Typography>
          </Stack>
          <div
            style={{
              background: "linear-gradient(90deg, #ffc029 0%, #4cd8b0 100%)",
              width: "100%",
              height: "7px",
              alignSelf: "flex-end",
            }}
          ></div>
        </Link>
      </Card>
    </>
  );
}
