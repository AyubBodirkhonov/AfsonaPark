import { Typography, Stack, Container, Box } from "@mui/material";
import { Link } from "react-router-dom";
import Slide from "react-reveal/Slide";

import "./HeaderContent.style.scss";

export default function headerContent({ backText, mainText, link, title }) {
  return (
    <Box className="header-content">
      <Box className="header-img-box"></Box>

      <Box className="header-wrap-container">
        <Slide left>
          <Typography className="header-wrap-text">{backText}</Typography>
        </Slide>
      </Box>
      <Box className="header-text-container">
        <Slide top>
          <Typography
            variant="h3"
            sx={{
              color: "#fff",
              fontWeight: "700",
              fontFamily: "Fira Sans Condensed, sans-sarif",
              textAlign: "center",
              fontSize: { xs: "1.5rem", md: "3rem" },
            }}
            className="header-top-line"
            mb={2}
          >
            {mainText}
          </Typography>
        </Slide>
      </Box>
      <Stack direction="row" justifyContent="center" className="header-link">
        <Link
          style={{
            color: "#fff",
            fontWeight: "700",
            fontFamily: "Fira Sans Condensed, sans-sarif",
          }}
          to="/"
        >
          {link}
          <span style={{ padding: "20px" }}>&#x2022;</span>
        </Link>
        <Typography
          sx={{
            color: "#fff",
            fontWeight: "700",
            fontFamily: "Fira Sans Condensed, sans-sarif",
            fontSize: { xs: "1rem", md: "1.2rem" },
          }}
        >
          {" "}
          {title}
        </Typography>
      </Stack>
    </Box>
  );
}
