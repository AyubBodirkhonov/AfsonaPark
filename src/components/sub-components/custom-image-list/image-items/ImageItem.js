import React, { useState } from "react";
import { Box } from "@mui/material";

const ImageItem = ({ item, onClick }) => {
  const [isHovered, setIsHovered] = useState(false);

  const handleMouseEnter = () => {
    setIsHovered(true);
  };

  const handleMouseLeave = () => {
    setIsHovered(false);
  };
  return (
    <Box
      sx={{
        borderRadius: "16px",
        overflow: "hidden",
        height: { xs: "200px", md: "350px" },
        cursor: "pointer",
      }}
      onClick={onClick}
    >
      <img
        src={item.image}
        alt={item.title}
        style={{
          width: "100%",
          height: "100%",
          objectFit: "cover",
          filter: `brightness(${isHovered ? 1 : 0.9})`,
          transition: "0.3s ease-in-out",
          "&:hover": {
            filter: "brightness(1)",
          },
        }}
        onMouseEnter={handleMouseEnter}
        onMouseLeave={handleMouseLeave}
      />
    </Box>
  );
};

export default ImageItem;
