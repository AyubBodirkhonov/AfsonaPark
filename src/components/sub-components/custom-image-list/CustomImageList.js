import { useState } from "react";
import {
  Grid,
  Typography,
  Box,
  Tab,
  Container,
  IconButton,
  Modal,
} from "@mui/material";
import Tabs, { tabsClasses } from "@mui/material/Tabs";
import CloseIcon from "@mui/icons-material/Close";
import PropTypes from "prop-types";
import { useTheme } from "@mui/material/styles";
import { carouselItems } from "../../../data/data";
import TabPanel from "./tabpanel/TabPanel";
import ImageItem from "./image-items/ImageItem";
import Slide from "react-reveal/Slide";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: "90%",
  height: { xs: "50%", lg: "90%" },
  bgcolor: "background.paper",
  boxShadow: 24,
  outline: "none",
  backgroundAttach: "fixed",
  borderRadius: "15px",
};

const commonStyles = {
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-serif",
};

const imageStyle = {
  width: "100%",
  height: "100%",
  objectFit: "cover",
  borderRadius: "15px",
};

function CustomTabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

CustomTabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function CustomImageList() {
  const [value, setValue] = useState(0);
  const [selectedImage, setSelectedImage] = useState(null);
  const theme = useTheme();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleImageClick = (image) => {
    setSelectedImage(image);
  };

  const handleCloseModal = () => {
    setSelectedImage(null);
  };
  return (
    <>
      <Modal open={!!selectedImage} onClose={handleCloseModal}>
        <Box sx={style}>
          <img src={selectedImage} alt="Full Size" style={imageStyle} />

          <IconButton
            edge="end"
            onClick={handleCloseModal}
            aria-label="close"
            sx={{ position: "absolute", top: 0, right: 20, color: "white" }}
          >
            <CloseIcon />
          </IconButton>
        </Box>
      </Modal>
      <Box
        sx={{
          flexGrow: 1,
          bgcolor: "background.paper",
          overflow: "hidden",
          padding: { xs: "0", md: "1rem 1rem" },
        }}
      >
        <Box
          sx={{
            flexGrow: 1,
            maxWidth: { xs: 300, sm: 660 },
            bgcolor: "background.paper",
            margin: "1rem auto",
            marginTop: "2rem",
            padding: { md: "1.6rem" },
          }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            variant="scrollable"
            indicatorColor="primary"
            textColor="primary"
            allowScrollButtonsMobile
            aria-label="visible arrows tabs example"
            sx={{
              [`& .${tabsClasses.scrollButtons}`]: {
                "&.Mui-disabled": { opacity: 0.7 },
                color: "white",
                backgroundColor: "#002cae",
                borderRadius: "100%",
                width: { xs: "40px", md: "50px" },
                height: { xs: "40px", md: "50px" },
              },
            }}
          >
            <Tab label="Aттракционы" {...a11yProps(0)} sx={commonStyles} />
            <Tab label="VR-зона" {...a11yProps(1)} sx={commonStyles} />
            <Tab label="Aквапарк" {...a11yProps(2)} sx={commonStyles} />
            <Tab label="Квадроциклы" {...a11yProps(3)} sx={commonStyles} />
            <Tab label="Квадроциклы" {...a11yProps(4)} sx={commonStyles} />
          </Tabs>
        </Box>
        <TabPanel value={value} index={0}>
          <Grid container spacing={{ xs: 2, md: 3 }}>
            {carouselItems.map((item, index) => (
              <Grid item xs={6} md={index === 0 ? 8 : 4} key={index}>
                <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                  <ImageItem
                    item={item}
                    onClick={() => handleImageClick(item.image)}
                  />
                </Slide>
              </Grid>
            ))}
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={1}>
          <Grid container spacing={3}>
            {carouselItems.map((item, index) => (
              <Grid item xs={6} md={index === 0 ? 8 : 4} key={index}>
                <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                  <ImageItem
                    item={item}
                    onClick={() => handleImageClick(item.image)}
                  />
                </Slide>
              </Grid>
            ))}
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={2}>
          <Grid container spacing={3}>
            {carouselItems.map((item, index) => (
              <Grid item xs={6} md={index === 0 ? 8 : 4} key={index}>
                <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                  <ImageItem
                    item={item}
                    onClick={() => handleImageClick(item.image)}
                  />
                </Slide>
              </Grid>
            ))}
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={3}>
          <Grid container spacing={3}>
            {carouselItems.map((item, index) => (
              <Grid item xs={6} md={index === 0 ? 8 : 4} key={index}>
                <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                  <ImageItem
                    item={item}
                    onClick={() => handleImageClick(item.image)}
                  />
                </Slide>
              </Grid>
            ))}
          </Grid>
        </TabPanel>
        <TabPanel value={value} index={4}>
          <Grid container spacing={3}>
            {carouselItems.map((item, index) => (
              <Grid item xs={6} md={index === 0 ? 8 : 4} key={index}>
                <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                  <ImageItem
                    item={item}
                    onClick={() => handleImageClick(item.image)}
                  />
                </Slide>
              </Grid>
            ))}
          </Grid>
        </TabPanel>
      </Box>
    </>
  );
}
