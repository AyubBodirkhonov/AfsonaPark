import { Typography } from "@mui/material";

const typoStyle = {
  textAlign: "center",
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  color: "#323268",
  position: "relative",
  fontSize: { xs: "2rem", lg: "3rem" },
  "::before": {
    content: '""',
    position: "absolute",
    top: "-35%",
    left: { xs: "25%", md: "0" },
    height: "4px",
    width: { xs: "50%", md: "35%" },
    borderRadius: "5px",
    background:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%);",
  },
};
export default function CustomTypography({ title, ...props }) {
  return (
    <>
      <Typography variant="h4" sx={{ ...typoStyle }} {...props}>
        {title}
      </Typography>
    </>
  );
}
