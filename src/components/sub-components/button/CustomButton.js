import { Button } from "@mui/material";
import "./CustomButton.style.scss";

export default function CustomButton({ children, ...props }) {
  return (
    <Button {...props} className="btn">
      {children}
    </Button>
  );
}
