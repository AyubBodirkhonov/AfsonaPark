import { Box, Grid } from "@mui/material";
import Slide from "react-reveal/Fade";
import { myNews } from "../../data/data";
import CustomCard from "../sub-components/card/CustomCard";

import "./SectionNews.style.scss";
import CustomButton from "../sub-components/button/CustomButton";
import CustomTypography from "../sub-components/custom-typo/CustomTypography";
import { Link } from "react-router-dom";

const buttonStyle = {
  padding: { xs: "10px 24px", sm: "14px 24px" },
  boxShadow: "0 4px 15px 0 rgba(126, 52, 161, 0.75)",
  borderRadius: "24px",
  fontSize: { xs: "0.7rem" },
  width: { xs: "100%", md: "auto" },
  marginRight: { md: "1rem" },
  display: { xs: "none", md: "block" },
};

export default function SectionNews() {
  const selectedNews = myNews.slice(0, 3);

  return (
    <Box className="section-news">
      <Box sx={{ padding: { xs: "10% 10%", sm: "0 3%", md: "0 20%" } }}>
        <Slide top>
          <Box
            sx={{
              position: "relative",
              display: "flex",
              justifyContent: "space-between",
              alignItems: "center",
              flexDirection: { xs: "column", md: "row" },
              gap: { xs: "15px", md: "0" },
              margin: { sm: "2% 10%", md: "5% auto" },
            }}
          >
            <span className="wrap-news">Новости</span>
            <CustomTypography title="Новости" my={5} />
            <Link to="/news">
              <CustomButton
                variant="contained"
                sx={{
                  ...buttonStyle,
                }}
              >
                <strong>Все новости</strong>
              </CustomButton>
            </Link>
          </Box>
        </Slide>
        <Grid
          container
          justifyContent="center"
          spacing={{ xs: 1, sm: 4, md: 1 }}
        >
          {selectedNews.map((item, index) => (
            <Grid item xs={12} sm={5} md={4} lg={4} key={index}>
              <Slide right={index % 2 === 0} bottom={index % 2 === 1}>
                <CustomCard news={item} />
              </Slide>
            </Grid>
          ))}
        </Grid>
        <Box mt={5}>
          <Link to="/gallery" style={{ textAlign: "center" }}>
            <CustomButton
              variant="contained"
              sx={{
                display: { xs: "block", md: "none" },
                boxShadow: buttonStyle.boxShadow,
                padding: buttonStyle.padding,
                borderRadius: buttonStyle.borderRadius,
                fontSize: buttonStyle.fontSize,
                margin: "0 auto",
                width: { xs: "50%", sm: "30%" },
              }}
            >
              <strong> Все новости</strong>
            </CustomButton>
          </Link>
        </Box>
      </Box>
    </Box>
  );
}
