import React from "react";
import { Box, Grid } from "@mui/material";
import { Link } from "react-router-dom";
import Slide from "react-reveal/Fade";
import CustomButton from "../../components/sub-components/button/CustomButton";
import GalleryItem from "../sub-components/gallery-item/GalleryItem"; // New component for gallery items
import { galleryData } from "../../data/data"; // Example data for gallery images
import "./SectionGallery.style.scss";
import CustomTypography from "../sub-components/custom-typo/CustomTypography";

///////////////////// Custom Styles

const buttonStyle = {
  padding: { xs: "10px 54px", sm: "16px 24px" },
  boxShadow: "0 4px 15px 0 rgba(126, 52, 161, 0.75)",
  borderRadius: "24px",
  fontSize: { xs: "0.7rem" },
  width: { xs: "100%", sm: "50%", md: "100%" },
  display: { xs: "none", md: "block" },
};

const flexBox = {
  position: "relative",
  display: "flex",
  justifyContent: "space-between",
  alignItems: "center",
  flexDirection: { xs: "column", md: "row" },
  gap: { xs: "15px", md: "0" },
};

export default function SectionGallery() {
  return (
    <Box
      sx={{
        width: "100%",
        height: "auto",
        margin: { xs: "15% auto", sm: "8% auto" },
      }}
    >
      <Box
        sx={{
          position: "relative",
          margin: { xs: "10% 5%", sm: "0 3%", md: "0 20%" },
        }}
      >
        <Slide top>
          <Box
            my={4}
            sx={{
              ...flexBox,
            }}
          >
            <span className="wrap-gallery">Галерея</span>
            <CustomTypography title="Галерея" mt={3} />
            <Link to="/gallery" style={{ textAlign: "center" }}>
              <CustomButton variant="contained" sx={{ ...buttonStyle }}>
                <strong> Вся галерея</strong>
              </CustomButton>
            </Link>
          </Box>
        </Slide>
        <Grid container spacing={3}>
          {galleryData.map((item, index) => (
            <Grid
              item
              xs={index === 0 ? 12 : 6}
              sm={6}
              lg={index === 0 ? 8 : 4}
              key={index}
            >
              <Slide bottom={index % 2 === 0} right={index % 2 === 1}>
                <GalleryItem {...item} />
              </Slide>
            </Grid>
          ))}
        </Grid>
        <Box mt={5}>
          <Link to="/gallery" style={{ textAlign: "center" }}>
            <CustomButton
              variant="contained"
              sx={{
                display: { xs: "block", md: "none" },
                boxShadow: buttonStyle.boxShadow,
                padding: buttonStyle.padding,
                borderRadius: buttonStyle.borderRadius,
                fontSize: buttonStyle.fontSize,
                margin: "0 auto",
              }}
            >
              <strong> Вся галерея</strong>
            </CustomButton>
          </Link>
        </Box>
      </Box>
    </Box>
  );
}
