import { Card, Stack, Grid, Box, Typography } from "@mui/material";
import Slide from "react-reveal/Slide";
import { myGifs } from "../../data/data";
import "./SectionCards.style.scss";

const topLineStyle = {
  position: "relative",
  fontSize: { xs: "1.2rem", sm: "1.5rem", md: "1.8rem", lg: "2rem" },
  textAlign: "center",
  fontWeight: "700",
  fontFamily: "Fira Sans Condensed, sans-sarif",
  color: "#323268",

  "::before": {
    content: '""',
    position: "absolute",
    top: "-35%",
    left: { xs: "47%", sm: "45%", md: "47%" },
    height: "4px",
    width: { xs: "10%", md: "5%" },
    borderRadius: "5px",
    background:
      "linear-gradient(to left, #dc2424 0%, #4a569d 90%, #dc2424 120%);",
  },
};

const cardStyle = {
  width: { xs: "100%", lg: "70%" },
  height: "220px",
  padding: "15px 10px",
  borderRadius: "16px",
  display: "flex",
  alignItems: "center",
  flexDirection: "column",
  justifyContent: "space-around",
  boxShadow: "10px 10px 25px 0 #eef0f9",
  textAlign: "center",
};

export default function SectionCard() {
  return (
    <Box
      sx={{
        width: "100%",
        padding: { xs: "15% 0", md: "10% 2%", lg: "5% 0" },
      }}
    >
      <Slide top>
        <Box mb={5} sx={{ position: "relative" }}>
          <span className="wrap">Афсона Парк</span>
          <Typography
            variant="h4"
            sx={{
              ...topLineStyle,
            }}
          >
            Почему Стоит Посетить Всесезонный <br />
            Парк Развлечений Afsona Land
          </Typography>
        </Box>
      </Slide>

      <Grid
        container
        justifyContent="center"
        sx={{ paddingTop: "2%" }}
        spacing={2}
        p={{ xs: 2, md: 0 }}
      >
        {myGifs.map((gif, index) => (
          <Grid item key={index} xs={6} md={4} lg={2}>
            <Slide bottom={index % 2 === 0} right={index % 2 === 1}>
              <Card sx={{ ...cardStyle }}>
                <Box sx={{ width: "120px", height: "120px" }}>
                  <img
                    src={gif.gif}
                    alt={gif.title}
                    style={{ width: "100%", height: "100%" }}
                  />
                </Box>

                <Typography
                  variant="h7"
                  sx={{
                    textAlign: "center",
                    fontWeight: "700",
                    fontFamily: "Fira Sans Condensed, sans-sarif",
                  }}
                >
                  {gif.title}
                </Typography>
              </Card>
            </Slide>
          </Grid>
        ))}
      </Grid>
    </Box>
  );
}
