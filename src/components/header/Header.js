//Material UI imports
import { Box, Typography, Stack } from "@mui/material";
import { Link } from "react-router-dom";
//Components Imports
import Slide from "react-reveal/Slide";
import CustomButton from "../sub-components/button/CustomButton";
//Image Imports
import AfsonaLogoType from "../../assets/background/afsona-land-high-resolution-logo-white-transparent.png";
import VideoMp4 from "../../assets/video/IMG_0264.MP4";
// CSS import
import "./Header.style.scss";

export default function Header() {
  return (
    <Box className="header-container">
      <Box className="header-prev-container">
        <video autoPlay muted loop className="myVideo">
          <source src={VideoMp4} type="video/mp4" />
        </video>
      </Box>
      <Stack className="header-flex-box" alignItems="center" spacing={4}>
        <Box className="header-img-container">
          <Slide top>
            <img
              src={AfsonaLogoType}
              alt="AfsonaLogotype"
              style={{ width: "100%", height: "100%" }}
            />
          </Slide>
        </Box>
        <Slide bottom>
          <Typography
            variant="h4"
            sx={{
              color: "#fff",
              fontWeight: "700",
              fontFamily: "Fira Sans Condensed, sans-sarif",
              letterSpacing: "2px",
              fontSize: { xs: "1.3rem", md: "2.125rem" },
            }}
          >
            Oткрыт с 10:00 до 23:00
          </Typography>
        </Slide>
        <Slide top>
          <Link to="/about">
            <CustomButton
              variant="contained"
              sx={{
                padding: { xs: "10px 24px", sm: "16px 24px" },
                boxShadow: "0 4px 15px 0 rgba(126, 52, 161, 0.75)",
                borderRadius: "24px",
                fontSize: "14px",
              }}
            >
              <strong>Узнайте больше</strong>
            </CustomButton>
          </Link>
        </Slide>
      </Stack>
    </Box>
  );
}
